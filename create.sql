CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `loaded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `addr_from` varchar(255) NOT NULL,
  `addr_to` varchar(255) NOT NULL,
  `processed` enum('Y','N') NOT NULL DEFAULT 'N',
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE mailbox (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(255)
);

