# postfix2db

## prepare

### Mysql configuration
~~~
create database mail
grant all on mail.* to mail@localhost identified by 'mailpass';
~~~

create tables:
~~~
mysql -u mail -pmailpass < create.sql
~~~
Database structure can be any, the only requirements - all fields (maybe except message body) must have default values, 
so this statement would work:
~~~
INSERT INTO tablename (fieldname) VALUES ('message')
~~~

### Postfix configuration

add to /etc/postfix/master.cf:
~~~
postfix2db unix  -        n       n        -      50    pipe
    flags=R user=nobody argv=/home/xenon/repo/postfix2db/postfix2db.py --db test -u test -p test -a addr_from ${sender} -a addr_to ${recipient}
~~~

add to /etc/postfix/main.cf:
~~~
transport_maps = hash:/etc/postfix/transport
~~~

/etc/postfix/transport:
~~~
111@example.com	postfix2db:
~~~
or
~~~
example.com	postfix2db:
~~~

# Optional way
instead of using mysql transport maps, you can use this in main.cf:
~~~
local_transport = postfix2db
~~~
(BUT this will ignore system users, e.g. if cron will send mail to root, it will not be delivered usual way)

# For checking mailboxes
(optional)
~~~
apt-install postfix-mysql
~~~

main.cf:
~~~
mydestination = $myhostname, braconnier, localhost.localdomain, , localhost, example.com
local_recipient_maps = mysql:/etc/postfix/mysql_check_recipient.cf
transport_maps = mysql:/etc/postfix/mysql_transport.cf
~~~

master.cf:
~~~
postfix2db unix -    n    n    -    50    pipe
    flags=R user=mail argv=/usr/local/bin/postfix2db.py -pmailpass
~~~

/etc/postfix/mysql_transport.cf:
~~~
hosts           = 127.0.0.1

user            = mail
password        = mailpass
dbname          = mail

table           = mailbox
select_field    = 'postfix2db'
where_field     = email
~~~

/etc/postfix/mysql_check_recipient.cf:
~~~
dbname  =   mail
user = mail
password = mailpass
hosts = 127.0.0.1
query = SELECT 'OK' FROM mailbox WHERE email='%s'
~~~
