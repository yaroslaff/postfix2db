#!/usr/bin/python3

import argparse
import sys
import io
import MySQLdb
import itertools

parser = argparse.ArgumentParser(
    description='Load STDIN to database')

def_host = 'localhost'
def_db = 'mail'
def_user = 'mail'
def_pass = None
def_table = 'message'
def_field = 'message'

parser.add_argument('--host', default=def_host, metavar='HOST', help='database host ({})'.format(def_host))
parser.add_argument('--db', default=def_db, metavar='DATABASE', help='database name ({})'.format(def_db))
parser.add_argument('-t', default=def_table, dest='table', metavar='TABLE', help='table name ({})'.format(def_table))
parser.add_argument('-f', default=def_field, dest='field', metavar='FIELD', help='field name ({})'.format(def_field))
parser.add_argument('-u', default=def_user, dest='user', metavar='USER', help='database user ({})'.format(def_user))
parser.add_argument('-p', default=def_pass, dest='password', metavar='PASSWORD', help='database password ({})'.
                    format(def_pass))
parser.add_argument('-a', nargs=2, action='append', dest='additional', metavar=('FIELD', 'VALUE'),
                    help='additional fields')

args = parser.parse_args()

database = MySQLdb.connect(host=args.host, user=args.user, passwd=args.password, db=args.db, charset='utf8')
cursor = database.cursor()

input_stream = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8')
data = input_stream.read()

fieldlist = [args.field]
datalist = [data]
tpllist = ['%s']

if args.additional:
    for fname, fvalue in args.additional:
        fieldlist.append(fname)
        datalist.append(fvalue)
        tpllist.append('%s')


sql = "INSERT INTO {} ({}) VALUES ({})".format(args.table, ', '.join(fieldlist), ', '.join(tpllist))
cursor.execute(sql, datalist)
database.commit()
print(' '.join((itertools.chain.from_iterable(args.additional))))

